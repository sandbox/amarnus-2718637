<?php

/**
 * @file
 * Contains \Drupal\slackdown\Plugin\Filter\Slackdown.
 */

namespace Drupal\slackdown\Plugin\Filter;

use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Translates text written in Slack-style markdown to HTML.
 *
 * @Filter(
 *   id = "slackdown",
 *   module = "slackdown",
 *   title = @Translation("Slackdown"),
 *   description = @Translation("Allows content to be submitted using Slack-style markdown."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 *   cache = FALSE
 * )
 */
class Slackdown extends FilterBase {

  public function __construct() {
    // Let the parent constructor do its thing.
    $args = func_get_args();
    call_user_func_array(array($this, 'parent::__construct'), $args);
    // libraries_load('slackdown');
    $this->slackdown = new \Text\Slackdown($this->getOptions());
  }

  private function getOptions() {
    $options = array();
    foreach (array_keys(\Text\Slackdown::getFilters()) as $filter) {
      $key = "filter_slackdown_$filter";
      $options[$filter] = $this->settings[$key];
    }
    return $options;
  }

  public function settingsForm(array $form, FormStateInterface $form_state) {
    foreach (\Text\Slackdown::getFilters() as $filter => $label) {
      $key = "filter_slackdown_$filter";
      $form[$key] = array(
        '#type' => 'checkbox',
        '#title' => $label,
        '#default_value' => $this->settings[$key]
      );
    }
    return $form;
  }

  public function process($text, $langcode) {
    return new FilterProcessResult($this->slackdown->process($text));
  }

}
